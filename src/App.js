import './App.css';
import 'dotenv/config';
// import { useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Navbar from './components/Navbar';
import Home from './components/Home';
import SignUp from './components/SignUp';
import SignIn from './components/SignIn';
import BlogsList from './components/BlogsList';
import BlogDetails from './components/BlogDetails';
import Profile from './components/Profile';

function App() {
	const logOut = props => {
		localStorage.clear();
		props.history.push('/');
		window.location.reload('/');
	};

	return (
		<div className='App'>
			<Router>
				<Navbar logout={logOut} />
				<Switch>
					<Route path='/' exact component={Home} />
					<Route path='/sign-up' component={SignUp} />
					<Route path='/sign-in' component={SignIn} />
					<Route path='/blogs-list' component={BlogsList} />
					<Route path='/blog/:id' component={BlogDetails} />
					<Route path='/profile' component={Profile} />
				</Switch>
			</Router>
		</div>
	);
}

export default App;
