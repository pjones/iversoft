const Home = () => {
	return (
		<div className='pageWrapper'>
			<h1>Iversoft API usage instructions:</h1>
			<h3>Log In / Sign Up</h3>
			<p>
				To login, click on the Log In link in the top navigation and fill out
				the form using:{' '}
				<span className='green'>web@iversoft.ca / Ivers0ft</span>
			</p>
			<p>
				You will be logged in and redirected to the Blogs List page. The top
				navigation menu has also changed - because you are logged in. The Log In
				and Sign Up links have been removed and replaced with the Blogs List and
				Profile links.
			</p>
			<p>
				You can also create a new user by clicking on the Sign Up link. You need
				to provide a name, email address and password. When the form is
				submitted you are automatically logged in and redirected to the Blogs
				List page - just like the Log In functionality, but now you can reuse
				the user that you just created to Log In again.
			</p>
			<h3>Blogs List</h3>
			<p>
				The Blogs List page show a list of all of the available blog posts.
				Clicking on the individual blog posts takes you to the single blog post
				view. At the bottom of the blog post detail, there is a link back to the
				blog list view.
			</p>
			<h3>Profile</h3>
			<p>
				The Profile page shows the name, email address and the date/time the
				user was created. At the bottom of the profile page there is a logout
				button demonstrating another technique of adding functionality. Clicking
				on this button will rteurn you to this page and restore the top
				navigation menu items to the previous logged out state (Log In and Sign
				Up).
			</p>
			<h3>Other notes:</h3>
			<p>
				The User's profile and token are stored in localStorage and are removed
				when the logout button is clicked.
			</p>
			<p>
				The CSS for the Log In and Sign Up forms was done using styled
				components to demonstrate that technique. In the interest of time, the
				rest of the styling was added to App.css.
			</p>
			<p>
				Clicking on the logo will bring you back to this page regardless if you
				are logged in or not.
			</p>
		</div>
	);
};

export default Home;
