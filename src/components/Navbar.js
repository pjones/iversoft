import navLogo from '../img/full-colour.svg';
import { Link } from 'react-router-dom';
import { useState, useEffect } from 'react';

const Navbar = props => {
	const [userToken, setUserToken] = useState();

	useEffect(() => {
		let t = localStorage.getItem('token');
		if (t) {
			setUserToken(t);
		} else {
			localStorage.clear();
		}
	}, []);

	return (
		<nav className='navbar'>
			<Link to='/'>
				<img src={navLogo} alt='logo' className='logo' />
			</Link>
			<div className='nav-links'>
				{!userToken ? (
					<>
						<Link to='/sign-in' className='nav-link'>
							Log In
						</Link>
						<Link to='/sign-up' className='nav-link'>
							Sign Up
						</Link>
					</>
				) : (
					<>
						<Link to='/blogs-list' className='nav-link'>
							Blogs List
						</Link>
						<Link to='/profile' className='nav-link'>
							Profile
						</Link>
					</>
				)}
			</div>
		</nav>
	);
};

export default Navbar;
