import Form from './styles/Form';
import useForm from '../lib/useForm';
import { useState, useEffect } from 'react';

const SignUp = props => {
	console.log(props);
	const { inputs, handleChange, resetForm } = useForm({
		name: '',
		email: '',
		password: ''
	});
	const [newUser, setNewUser] = useState();

	async function handleSubmit(e) {
		e.preventDefault();
		console.log(inputs);
		let myHeaders = new Headers();
		myHeaders.append('Accept', 'application/json');
		myHeaders.append('mode', 'no-cors');
		myHeaders.append('Authorization', 'Bearer' + process.env.REACT_APP_API_KEY);
		myHeaders.append('Content-Type', 'application/x-www-form-urlencoded');

		var urlencoded = new URLSearchParams();
		urlencoded.append('name', inputs.name);
		urlencoded.append('email', inputs.email);
		urlencoded.append('password', inputs.password);

		var SignUpRequestOptions = {
			method: 'POST',
			headers: myHeaders,
			body: urlencoded,
			redirect: 'follow'
		};

		const request = async () => {
			const response = await fetch(
				`${process.env.REACT_APP_PROXY}/${process.env.REACT_APP_BASE_URL}/api/users/new`,
				SignUpRequestOptions
			);
			const json = await response.json();
			console.log(json);
			setNewUser(json);
			localStorage.setItem('token', json.token);
			localStorage.setItem('user', JSON.stringify(json.user));
		};

		request();
		resetForm();
	}

	useEffect(() => {
		if (newUser) {
			props.history.push('/blogs-list');
			window.location.reload();
		}
	}, [newUser, props.history]);

	return (
		<div className='formWrapper'>
			<Form method='POST' onSubmit={handleSubmit}>
				<h2>Sign Up For An Account</h2>
				<fieldset>
					<label htmlFor='name'>
						Your Name
						<input
							type='text'
							name='name'
							placeholder='Your Name'
							autoComplete='name'
							value={inputs.name}
							onChange={handleChange}
						/>
					</label>
					<label htmlFor='email'>
						Email
						<input
							type='email'
							name='email'
							placeholder='Your Email Address'
							autoComplete='email'
							value={inputs.email}
							onChange={handleChange}
						/>
					</label>
					<label htmlFor='password'>
						Password
						<input
							type='password'
							name='password'
							placeholder='Password'
							autoComplete='password'
							value={inputs.password}
							onChange={handleChange}
						/>
					</label>
					<button type='submit'>Sign Up!</button>
				</fieldset>
			</Form>
		</div>
	);
};

export default SignUp;
