import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

const baseURL = 'http://cms.iversoft.ca';

const BlogDetails = props => {
	const blogId = props.match.params.id;

	const [blog, setBlog] = useState();

	useEffect(() => {
		let myHeaders = new Headers();
		myHeaders.append('Accept', 'application/json');
		myHeaders.append('mode', 'no-cors');
		myHeaders.append('Authorization', 'Bearer' + process.env.REACT_APP_API_KEY);
		myHeaders.append('Content-Type', 'application/x-www-form-urlencoded');

		var blogListRequestOptions = {
			method: 'GET',
			headers: myHeaders,
			redirect: 'follow'
		};

		const request = async () => {
			const response = await fetch(
				`${process.env.REACT_APP_PROXY}/${baseURL}/api/blog/single/${blogId}`,
				blogListRequestOptions
			);
			const json = await response.json();
			// console.log(json);
			setBlog(json);
		};
		request();
	}, [blogId]);

	return (
		<div>
			<h1>Blog {blogId}</h1>
			<div className='blog-post'>
				{!blog ? (
					''
				) : (
					<div>
						<h3 className='title'>Title: {blog.title}</h3>
						<p className='author'>Author: {blog.author_user.name}</p>
						<p className='create-date'>Created: {blog.created_at}</p>
						<p className='blog-body'>Description: {blog.content}</p>
					</div>
				)}
			</div>
			<Link to='/blogs-list' className='btnStyled'>
				Back to Blogs List
			</Link>
		</div>
	);
};

export default BlogDetails;
