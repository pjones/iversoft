import { useState, useEffect } from 'react';

const baseURL = 'http://cms.iversoft.ca';

const BlogsList = () => {
	const [blogs, setBlogs] = useState();

	useEffect(() => {
		let myHeaders = new Headers();
		myHeaders.append('Accept', 'application/json');
		myHeaders.append('mode', 'no-cors');
		myHeaders.append('Authorization', 'Bearer' + process.env.REACT_APP_API_KEY);
		myHeaders.append('Content-Type', 'application/x-www-form-urlencoded');

		var blogListRequestOptions = {
			method: 'GET',
			headers: myHeaders,
			redirect: 'follow'
		};

		(async () => {
			let response = await fetch(
				`${process.env.REACT_APP_PROXY}/${baseURL}/api/blog/list`,
				blogListRequestOptions
			);
			let data = await response.json();
			// console.log(data.data);
			const blogData = data.data;
			setBlogs(blogData);
		})();
	}, []);

	return (
		<div className='pageWrapper'>
			<h1>Blogs List</h1>
			{!!blogs &&
				blogs.map(blog => {
					return (
						<a href={`/blog/${blog.id}`} key={blog.id}>
							<div className='blog-card'>
								<p>{blog.title}</p>
							</div>
						</a>
					);
				})}
		</div>
	);
};

export default BlogsList;
