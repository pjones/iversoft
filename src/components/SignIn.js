import Form from './styles/Form';
import useForm from '../lib/useForm';
import { useState, useEffect } from 'react';

export default function SignIn(props) {
	const { inputs, handleChange, resetForm } = useForm({
		email: '',
		password: ''
	});
	const [authToken, setAuthToken] = useState();

	async function handleSubmit(e) {
		e.preventDefault();

		let myHeaders = new Headers();
		myHeaders.append('Accept', 'application/json');
		myHeaders.append('mode', 'no-cors');
		myHeaders.append('Authorization', 'Bearer' + process.env.REACT_APP_API_KEY);
		myHeaders.append('Content-Type', 'application/x-www-form-urlencoded');

		var urlencoded = new URLSearchParams();
		urlencoded.append('email', inputs.email);
		urlencoded.append('password', inputs.password);

		var SignInRequestOptions = {
			method: 'POST',
			headers: myHeaders,
			body: urlencoded,
			redirect: 'follow'
		};

		const request = async () => {
			const response = await fetch(
				`${process.env.REACT_APP_PROXY}/${process.env.REACT_APP_BASE_URL}/api/authenticate`,
				SignInRequestOptions
			);
			const json = await response.json();
			console.log(json);
			setAuthToken(json.token);
			localStorage.setItem('token', json.token);
			localStorage.setItem('user', JSON.stringify(json.user));
		};

		request();
		resetForm();
	}

	useEffect(() => {
		if (authToken) {
			props.history.push('/blogs-list');
			window.location.reload();
		}
	}, [authToken, props.history]);

	return (
		<div className='formWrapper'>
			<Form method='POST' onSubmit={handleSubmit}>
				<h2>Login To Your Account</h2>
				<fieldset>
					<label htmlFor='email'>
						Email
						<input
							type='email'
							name='email'
							placeholder='Your Email Address'
							autoComplete='email'
							value={inputs.email}
							onChange={handleChange}
						/>
					</label>
					<label htmlFor='password'>
						Password
						<input
							type='password'
							name='password'
							placeholder='Password'
							autoComplete='password'
							value={inputs.password}
							onChange={handleChange}
						/>
					</label>
					<button type='submit'>Sign In!</button>
				</fieldset>
			</Form>
		</div>
	);
}
