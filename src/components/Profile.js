const Profile = props => {
	const userData = JSON.parse(localStorage.getItem('user'));

	const logOut = () => {
		localStorage.clear();
		props.history.push('/');
		window.location.reload();
	};

	return (
		<>
			<h1>Your Profile</h1>
			<p>Name: {userData.name}</p>
			<p>Email: {userData.email}</p>
			<p>Signed up on: {userData.created_at}</p>
			<button onClick={logOut}>logout</button>
		</>
	);
};

export default Profile;
