# Getting Started with the Iversoft API interface

You will need to have nodejs installed on your computer.

Once you have downloaded the repo, you need to install the app's dependencies with

### `npm install`

from within the repo's directory on the command line.

## Available Scripts

Before running the app with the following script, you will need to add a file named .env to the root of this directory. I have emailed a copy of this file to Steph - add env.txt to the root of this repo and rename it to '.env'- no quotes but with the dot (period/full stop).

In the project directory, you can now run:

### `npm run start`

Runs the app in the development mode.

Open [http://localhost:8081](http://localhost:8081) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

## Learn More

There are instructions on how the interface works on the homepage of the app.

### Deployment

This app is live here: [https://hungry-bhabha-73dc0d.netlify.app/](https://hungry-bhabha-73dc0d.netlify.app/)
